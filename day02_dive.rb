input_instructions = "forward 5,down 5,forward 8,up 3,down 8,forward 2"
instructions = input_instructions.split(",")
x_position = 0
y_position = 0
instructions.each do |instruction|
  direction,steps = instruction.split(" ")
  case direction
    when "forward"
      x_position += steps.to_i
    when "up"
      y_position -= steps.to_i
    when "down"
      y_position += steps.to_i
  end
end

puts x_position * y_position
