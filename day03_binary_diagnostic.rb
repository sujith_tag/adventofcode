input = ["00100","11110","10110","10111","10101","01111","00111","11100","10000","11001","00010","01010"]

gamma_rate = ""
epsilon_rate = ""

input.first.length.times do |index|
  ordered_bits = input.map { |n| n[index] }
  if ordered_bits.count("1") > ordered_bits.count("0")
    gamma_rate += "1"
    epsilon_rate += "0"
  else
    gamma_rate += "0"
    epsilon_rate += "1"
  end
end

def binary_to_decimal(number)
	result = 0
	bit = 0
	n = number.length - 1
	while (n >= 0)
		if (number[n] == '1')
			result += (1 << (bit))
		end
		n = n - 1
		bit += 1
	end
	result.to_i
end

gamma = binary_to_decimal(gamma_rate)
epsilon = binary_to_decimal(epsilon_rate)

puts gamma * epsilon
