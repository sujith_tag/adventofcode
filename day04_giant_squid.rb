draw_list = []
completed = false

first_card = (0..24).to_a.shuffle.each_slice(5).to_a
second_card = (0..24).to_a.shuffle.each_slice(5).to_a
third_card = (0..24).to_a.shuffle.each_slice(5).to_a

cards = first_card,second_card,third_card

def print_cards(cards)
  cards.each_with_index { |card,card_index|
    puts "*"*10
    puts "CARD #{card_index +1}"
    puts "*"*10
    card.count.times {|i| p card[i]}
    puts ""
  }
end

def draw_check(cards, draw)
  card_results = []
  cards.each_with_index do |card, card_index|
    result = card
    card.each_with_index do |rows, row_index|
      rows.each_with_index do |column, column_index|
        if draw.include?(column)
          result[row_index][column_index] = "x"
        end
      end
    end
    if is_bingo(result)
      print_result(draw, result,card_index+1 )
    end
  end
end

def is_bingo(card_result)
  card_result.each_with_index do |rows, row_index|
    if rows.count("x") == 5
      return true
    end
    if card_result[row_index - row_index][row_index] == "x" && card_result[row_index + (1-row_index)][row_index] == "x" && card_result[row_index + (2-row_index)][row_index] == "x" && card_result[row_index + (3-row_index)][row_index] == "x" && card_result[row_index + (4-row_index)][row_index] == "x"
      return true
    end
  end
  return false
end

def print_result(draw, result, card_number)
  puts "*"*20
  puts "CARD #{card_number} WINS"
  puts "*"*20
  5.times {|i| p result[i]}
  unmarked_sum = result.flatten.map(&:to_i).reduce(0, :+)
  puts unmarked_sum * draw.last
  abort("Game Over!")
end

print_cards(cards)

until completed do
  puts "ENTER NUMBER FOR THE DRAW"
  draw_input = gets.chomp
  if  first_card.flatten.include?(draw_input.to_i) && !draw_list.include?(draw_input.to_i)
    draw_list.push(draw_input.to_i)
    draw_check(cards, draw_list)
  else
    puts "SORRY, INVALID INPUT *"
  end
end
